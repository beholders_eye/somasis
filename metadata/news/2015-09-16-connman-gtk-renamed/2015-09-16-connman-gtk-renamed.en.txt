Title: net-misc/connman-gtk was renamed to net-misc/connman-ui-gtk
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-09-16
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-misc/connman-gtk[~scm]

net-misc/connman-gtk was renamed to connman-ui-gtk, since that is what upstream
calls it.

Install net-misc/connman-gtk, then uninstall net-misc/connman-ui-gtk:

1. Take note of anything depending on it (though there probably is nothing)

    cave resolve \!net-misc/connman-ui-gtk

2. Install net-misc/connman-gtk

    cave resolve net-misc/connman-gtk

3. Reinstall anything mentioned in step 1. (you probably need to `cave sync`)

4. Uninstall net-misc/connman-ui-gtk.

    cave resolve \!net-misc/connman-ui-gtk

If anything breaks, you keep the pieces; make sure to do it in this order.

net-misc/connman-gtk is now a different package that provides similar functionality.
