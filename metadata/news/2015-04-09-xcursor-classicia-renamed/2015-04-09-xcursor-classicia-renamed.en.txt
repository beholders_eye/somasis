Title: xcursor-classicia++ has been renamed to classicia-cursor-theme
Author: Kylie McClain <somasis@exherbo.org>
Content-Type: text/plain
Posted: 2015-04-09
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: x11-themes/xcursor-classicia++

I renamed x11-themes/xcursor-classicia++ in order to make it more consistent
with the other cursor theme package names.

Please install x11-themes/classicia-cursor-theme, and remove xcursor-classicia++
after you have done that.


