# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='yrutschle' tag="v${PV}" ]
require systemd-service [ systemd_files=[ scripts/sslh.service ] ]

SUMMARY="Applicative Protocol Multiplexer (e.g. share SSH and HTTPS on the same port)"

SLOT="0"
LICENCES="GPL-2"

PLATFORMS="~amd64"

MYOPTIONS="systemd"

DEPENDENCIES="
    build+run:
        dev-libs/libconfig
        dev-libs/pcre
        sys-apps/tcp-wrappers
        sys-libs/libcap
        systemd? ( sys-apps/systemd )
    test:
        dev-lang/perl:*
        dev-perl/IO-Socket-INET6
        dev-util/valgrind
"

DEFAULT_SRC_INSTALL_PARAMS=(
    BINDIR=/usr/$(exhost --target)/bin
)

src_prepare() {
    # fix localhost address used in tests
    edo sed -i 's/ip6-localhost/::1/g' ./t
    edo sed -r  \
            -e 's/USE([A-Z]*)=/USE\1?=/'    \
            -i Makefile
    default
}

src_compile() {
    # parallel appears broken
    emake -j1 \
        USELIBCONFIG=1      \
        USELIBPCRE=1        \
        USELIBCAP=1         \
        $(optionq systemd && echo USESYSTEMD=1)
}

src_test() {
    esandbox allow_net LOOPBACK6@{9000..9003}
    esandbox allow_path /tmp
    default
    esandbox disallow_path /tmp
    esandbox disallow_net LOOPBACK6@{9000..9003}
}

src_install() {
    default

    edo mv scripts/systemd.sslh.service scripts/sslh.service
    install_systemd_files
    insinto /etc
    insopts -m644
    doins "${FILES}"/${PN}.conf

    edo gunzip "${IMAGE}"/usr/share/man/man8/*.gz
}
