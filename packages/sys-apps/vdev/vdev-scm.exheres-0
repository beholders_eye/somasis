# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
#
# Parts derived from "systemd.exlib" from ::arbor, which is:
#   Copyright 2010-2015 Wulf C. Krueger <philantrop@exherbo.org>
#   Distributed under the terms of the GNU General Public License v2
#
# Specifically, the commands in pkg_setup, pkg_postinst, and src_install.
#

require udev-rules
require github [ user='jcnelson' tag="v${PV}" ]

SUMMARY="Device-file manager and filesystem; drop-in replacement for udev"
SLOT="0"
LICENCES="
    ISC
    GPL-2 [[ note = vdevd/helpers/LINUX ]]
"

DEPENDENCIES="
    build:
        sys-fs/squashfs-tools
    run:
        sys-apps/kmod
        !sys-apps/eudev [[
            description = [ ${CATEGORY}/${PN} is a udev daemon which provides the same library and programs that systemd-udev does ]
            resolution = manual
        ]]
        !sys-apps/systemd [[
            description = [ ${CATEGORY}/${PN} is a udev daemon which provides the same library and programs that systemd-udev does ]
            resolution = manual
        ]]
"

MYOPTIONS=""

PLATFORMS="~amd64"

MAKE_PARAMS=(
    AR=$(exhost --tool-prefix)ar
    CC=$(exhost --tool-prefix)cc
    CXX=$(exhost --tool-prefix)c++
    LD=$(exhost --tool-prefix)ld
    OSTYPE=LINUX

    DESTDIR="${IMAGE}"
    PREFIX=/usr/$(exhost --target)
    SBINDIR=/usr/$(exhost --target)/bin
    SHAREDIR=/usr/share
    PKGCONFIG=/usr/$(exhost --target)/lib/pkgconfig
)

pkg_setup() {
    exdirectory --allow /etc/udev/rules.d
}

src_compile() {
    emake -C hwdb ${MAKE_PARAMS[@]}
    emake -C vdevd ${MAKE_PARAMS[@]}
    emake -C libudev-compat ${MAKE_PARAMS[@]}
    emake -C example ${MAKE_PARAMS[@]} PREFIX=
}

src_install() {
    emake -C hwdb ${MAKE_PARAMS[@]} install
    emake -C vdevd ${MAKE_PARAMS[@]} install
    emake -C libudev-compat ${MAKE_PARAMS[@]} install
    emake -C example ${MAKE_PARAMS[@]} PREFIX= ETCDIR=/etc DESTDIR="${IMAGE}" install

    for file in "${IMAGE}"/usr/$(exhost --target)/lib/vdev/{*.sh,daemonlet};do
        edo sed -e "s|^#!/bin/dash|#!/bin/sh|"   \
                -i "${file}"
    done

    # Keep the udev rules, hwdb, etc.
    keepdir /usr/$(exhost --target)/lib/udev/devices
    keepdir /etc/udev/hwdb.d
    keepdir /etc/udev/rules.d

    # Add rules to the CONFIG_PROTECT mask
    hereenvd 20udev <<EOF
CONFIG_PROTECT_MASK="${UDEVRULESDIR}"
EOF
}

#pkg_postinst() {
#    default
#
#    # Restart the udev daemon if we are native, and running in the root it's installed to
#    if [[ -r /proc/1/root && /proc/1/root -ef /proc/self/root/ ]]; then
#        if exhost --is-native -q && [[ ${ROOT} == / ]]; then
#            nonfatal edo pkill udevd
#            nonfatal edo sleep 1
#            nonfatal edo pkill -9 udevd
#
#            case "$(esandbox api)" in
#                1)
#                    esandbox exec /usr/$(exhost --target)/bin/udevd --daemon || ewarn "udevd couldn't be restarted"
#                ;;
#                0)
#                    esandbox wait_eldest
#                    esandbox allow_net unix:/run/udev/control
#                    nonfatal edo /usr/$(exhost --target)/bin/udevd --daemon || ewarn "udevd couldn't be restarted."
#                ;;
#            esac
#        fi
#    fi
#    nonfatal edo /usr/$(exhost --target)/bin/udevadm hwdb --root="${ROOT}" --update || ewarn "Updating hwdb failed (udevadm hwdb --update)"
#}
