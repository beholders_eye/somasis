# Copyright 2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=somasis ]
require alternatives

HOMEPAGE="https://www.exherbo.org"
SUMMARY="Policy for the s6 init daemon"

SLOT="0"
LICENCES="ISC"

PLATFORMS="~amd64"

DEPENDENCIES="
    run:
        sys-apps/s6
        sys-apps/s6-linux-utils
        sys-apps/s6-rc
"

MYOPTIONS=""

DEFAULT_SRC_INSTALL_PARAMS=(
    exec_prefix=/usr/$(exhost --target)
)

src_install() {
    default

    keepdir /etc/s6-rc

    # s6-{reboot,poweroff,halt} are provided by s6-linux-utils
    alternatives_for    init s6 25  \
                        /usr/$(exhost --target)/bin/init        s6-init         \
                        /usr/$(exhost --target)/bin/reboot      s6-reboot       \
                        /usr/$(exhost --target)/bin/halt        s6-halt         \
                        /usr/$(exhost --target)/bin/poweroff    s6-poweroff
}

pkg_postinst() {
    if [[ ! -e /usr/$(exhost --target)/lib/s6/init-run/service/log/fifo ]];then
        edo mkfifo -m 0622 /usr/$(exhost --target)/lib/s6/init-run/service/log/fifo
        edo chown nobody:nobody /usr/$(exhost --target)/lib/s6/init-run/service/log/fifo
    fi
    alternatives_pkg_postinst
}

pkg_prerm() {
    edo rm -f /usr/$(exhost --target)/lib/s6/init-run/service/log/fifo
}

